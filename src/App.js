import React, {Component} from 'react';

import { Spin, Layout, Menu } from "antd";
import Header from "./components/Header";

import { BrowserRouter, Route, Link, Switch, Redirect } from "react-router-dom";
import { PrivateRoute } from './utils/privateRoute';

import HomePage from './pages/Home';
import LoginPage from "./pages/Login";
import About from "./pages/About";

import s from './App.module.scss'
import TestContext from "./context/testContext";
import FirebaseContext from "./context/FirebaseContext";
import MenuItem from "antd/es/menu/MenuItem";
import CurrentCard from "./pages/CurrentCard";

import { connect } from 'react-redux';
import {bindActionCreators} from "redux";
import {addUserAction} from "./actions/userAction";

class App extends Component {

    componentDidMount() {
        console.log('>>??>> context ', this.context);
        const { auth, setUserUid, errorMsg } = this.context;
        const { addUser } = this.props;
        auth.onAuthStateChanged(user => {
            console.log('::> onAuthChange');
            if (user) {
                setUserUid(user.uid);
                localStorage.setItem('user', JSON.stringify(user.uid));
                addUser(user);
            } else {
                setUserUid(null);
                localStorage.removeItem('user');
            }
        });
    }

    render() {
        const { userUid, err } = this.props;

        if (userUid === null) {
            return (
                <div className={s.loader_wrap}>
                    <Spin size="large" />
                </div>
            );
        }
/*
            <>
{
                    user ? (
                        <TestContext.Provider value={{uid: user.uid,}}>
                            <HomePage user={user}/>
                        </TestContext.Provider>
                    ) : <LoginPage />
                }
            </>
*/
        return (
            <>
                <Switch>
                <Route path="/login" component={LoginPage} />
                <Route render={(props) => {
                    console.log('app props: ', props);
                    const { history: {push} } = props;
                    return (
                        <Layout>
                            <Header classname={s.unheader}>
                                <Menu theme="dark" mode="horizontal">
                                    <Menu.Item key="1"><Link to="/">Main</Link></Menu.Item>
                                    <Menu.Item key="2"><Link to="/about">About</Link></Menu.Item>
                                    <Menu.Item key="3"><Link to="/login">Login</Link></Menu.Item>
                                    <Menu.Item key="4"><Link to="/contact" onClick={() => push('/contact')}>Contact</Link></Menu.Item>
                                </Menu>
                            </Header>
                            <div>
                                <Switch>
                                    <PrivateRoute path="/:id?/:isDone?" exact component={HomePage}/>
                                    <PrivateRoute path="/home/:id?/:isDone?" component={HomePage}/>
                                    <PrivateRoute path="/about" component={About}/>
                                    <PrivateRoute path="/word/:id?/:isDone?" component={CurrentCard}/>
                                    <Redirect to="/"/>
                                </Switch>
                            </div>
                        </Layout>
                    );
                }} />
                </Switch>
            </>
        );
    }
}

App.contextType = FirebaseContext;

const mapStateToProps = (state) => {
    return {
        userUid: state.user.userUid,
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        addUser: addUserAction,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

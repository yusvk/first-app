import React from "react";

const BackgroundBlock = ({color = 'white', children}) => {
    const background = color ? {backgroundColor: color} : {};

    return (
        <div className="background-block" style={background}>
            {children}
        </div>
    );
}

export default BackgroundBlock;
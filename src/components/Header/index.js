import React from 'react';
import s from './Header.module.scss'

const Header = ({classname = '', children}) => {
    // const changeDefault = {};
    // changeDefault.fontSize = size ? size + 'px' : null;
    // changeDefault.color = color ? color : null;
    // changeDefault.position = position ? position : null;
    // changeDefault.top = top ? top : null;
    //
    // console.log(changeDefault);

    return (<div className={s.header + ' ' + classname}>{children}</div>);
}

export default Header;
import React, { Component } from 'react';
import cl from 'classnames';
import s from './Card.module.scss';
import {
    CheckSquareOutlined,
    DeleteOutlined
} from "@ant-design/icons";
import { withRouter } from 'react-router-dom';

class Card extends Component {

    state = {
        done: false,
        isRemembered: false,
        isChecked: false
    }

    componentDidMount() {
        console.log(':::: card >> ', this.props);
        const { match : { params }, index} = this.props;
console.log('card>> ', index);
        if (index === +params.id) {
            this.setState({
                done: params.isDone,
            });
        }
    }

    handleCardClick = () => {
        this.setState(({done, isRemembered}) => {
            if (!isRemembered) {
                return {
                    done: !done
                }
            }
        });
    }

    handleIsRememberClick = () => {
        this.setState( ({done, isRemembered, isChecked}) => {
            if (!isRemembered) {
                return {
                    isRemembered: !isRemembered,
                    done: done ? done : !done,
                    isChecked: !isChecked,
                }
            }
        });
    }

    handleDeletedClick = () => {
        this.props.onDeleted();
    }

    render() {
        const { eng, rus } = this.props;
        const { done, isRemembered, isChecked } = this.state;

        return (
            <div className={s.root}>
                <div
                    className={ cl(s.card, {
                        [s.done] : done,
                        [s.isRemembered] : isRemembered,
                    }) }
                    onClick={ this.handleCardClick }
                >
                    <div className={s.cardInner}>
                        <div className={s.cardFront}>
                            { eng }
                        </div>
                        <div className={s.cardBack}>
                            { rus }
                        </div>
                    </div>
                </div>
                <div className={ cl(s.icons, {
                    [s.isChecked] : isChecked
                }) }>
                    <CheckSquareOutlined onClick={ this.handleIsRememberClick } />
                </div>
                <div className={s.icons}>
                    <DeleteOutlined onClick={ this.handleDeletedClick } />
                </div>
            </div>

        );
    }
}
export default withRouter(Card);
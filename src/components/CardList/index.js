import React, { Component } from 'react';
import s from './CardList.module.scss'
import Card from "../Card";
import Header from "../Header";
import getTranslateWord from "../../services/dictionary";

import { Input } from 'antd';

const { Search } = Input;

class CardList extends React.Component {
    state = {
        value: '',
        label: '',
        isBusy: false,
    }

    handleInputChange = (e) => {
        this.setState( {
            value: e.target.value,
        });
    }

    getTheWord = async () => {
        const { value } = this.state;
        const getWord = await getTranslateWord(this.state.value);

        this.setState(({value}) => {
            this.props.onAddItem(getWord);
            return {
                label: `${value} - ${getWord.translate}`,
                value: '',
                isBusy: false,
            };
        });
    }

    handleSubmitForm = async () => {
        this.setState({
            isBusy: true,
        }, this.getTheWord);
    }

    render () {
        const {item, onDeletedItem, inputRef} = this.props;
        const {value, label, isBusy} = this.state;

        return (
            <>
                <div className={s.cover}>
                    <div className={s.form}>
                        <div>
                            { label }
                        </div>
                        <br/>
                        <Search
                            placeholder="Input search text"
                            enterButton="Search"
                            size="large"
                            value={value}
                            loading={isBusy}
                            onChange={this.handleInputChange}
                            onSearch={this.handleSubmitForm}
                            ref={inputRef}
                        />
                    </div>
                    <Header classname={s.header} >Нажимая на карточки вы сможете прочесть перевод</Header>
                    <div className={s.root}>
                        {
                            item.map(({eng, rus, id}) =>
                                <Card onDeleted={() => {
                                    onDeletedItem(id);
                                }} key={id} eng={eng} rus={rus}/>)
                        }
                    </div>
                </div>
            </>
        );
    }
}

export default CardList;
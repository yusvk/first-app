import React from "react";

class FooterBlock extends React.Component {

    render() {
        const {color, lasttext} = this.props;
        const background = color ? {backgroundColor: color} : {};
        return (
            <div className="footer" style={background}>
                {lasttext && <h3>{lasttext}</h3>}
            </div>
        );
    }
}

export default FooterBlock;
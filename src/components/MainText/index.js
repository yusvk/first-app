import React from "react";
import {ReactComponent as ReactLogoSvg} from "../../logo.svg";
import s from './MainText.module.scss'

const MainText = ({header, text, padding, background = "default", children}) => {
    const paddingText = padding ? {padding: `${padding}px`} : {}

    return (
        <div className={s.default}>
            {header && <h1>{header}</h1>}
            {text && <div className="main-text" style={paddingText}>{text}</div>}
            {children}
        </div>
    );
}

export default MainText;
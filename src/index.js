import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, bindActionCreators, applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import * as actions from './actions';
import rootReducers from './reducers';
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { logger } from "redux-logger";
import thunk from "redux-thunk";
import 'antd/dist/antd.css';
import './index.css';
import FirebaseContext from "./context/FirebaseContext";
import Firebase from "./services/firebase";

const store = new createStore(rootReducers, applyMiddleware(thunk, logger));
const { dispatch } = store;

ReactDOM.render(
    <Provider store={store}>
        <FirebaseContext.Provider value={new Firebase()}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </FirebaseContext.Provider>
    </Provider>, document.getElementById('root')
);
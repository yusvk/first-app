import React, { Component } from "react";
import HeaderBlock from "../../components/HeaderBlock";
import Header from "../../components/Header";
import Paragraph from "../../components/Paragraph";
import CardList from "../../components/CardList";
import BackgroundBlock from "../../components/BackgroundBlock";
import {
    HomeOutlined, LoadingOutlined,
    SettingFilled,
    SmileOutlined,
    StarFilled,
    StarOutlined,
    StarTwoTone,
    SyncOutlined
} from "@ant-design/icons";
import  {Button} from "antd";
import {ReactComponent as ReactLogo} from "../../logo.svg";
import MainText from "../../components/MainText";
import FooterBlock from "../../components/FooterBlock";
import FirebaseContext from "../../context/FirebaseContext";

import s from './Home.module.scss';
import TestContext from "../../context/testContext";
import App from "../../App";
import { connect } from "react-redux";
import * as actions from '../../actions';

import {bindActionCreators} from "redux";


import {fetchCardList} from "../../actions/cardListAction";

class HomePage extends Component {

    inputRef = React.createRef();

    componentDidMount() {
        const { getUserCardsRef } = this.context;
        const {
            fetchCardList,
        } = this.props;

        fetchCardList(getUserCardsRef);
    }

    handleDeletedItem = (id) => {
        const { wordArr } = this.props;
        const { getUserCardsRef } = this.context;

        const newWordArr = wordArr.filter(item => item.id !== id);

        fetchCardList(getUserCardsRef().set(newWordArr));
    }

    setNewWord = (obj) => {
        const { getUserCardsRef } = this.context;
        const  { wordArr } = this.props;
        // max Id:
        let maxId = 0;

        if (wordArr.length > 0) {
            maxId = wordArr.map((item, tempId = null) => {
                return item.id;
            }).sort((a, b) => {
                return b - a;
            }).shift();
        }

        getUserCardsRef().set([...wordArr, {
            id: ++maxId,
            rus: obj.translate,
            eng: obj.text
        }], (res) => {
            fetchCardList(getUserCardsRef);
        });
    }

    logOut = () => {
        const { logOut, setUserUid } = this.context;
        const { history } = this.props;
        setUserUid(null);
        localStorage.removeItem('user');
        logOut().then(() => {history.push('/login')});
    }

    render() {
        const {
            countNumber,
            plusAction,
            minusAction,
            wordArr,
        } = this.props;
        console.log('HoMe::> ', this.props);

        return (
            <>
                <HeaderBlock>
                    <Paragraph>
                        <Button onClick={() => {
                            this.logOut();
                            console.log('click:> ');
                        }}>
                            Выйти из учетной записи
                        </Button>
                    </Paragraph>
                    <Header>Время учить слова онлайн</Header>
                    <Header>{ countNumber }</Header>
                    <Button onClick={() => plusAction(1)}>PLUS</Button>
                    <Button onClick={() => minusAction(1)}>MINUS</Button>
                    <Paragraph>
                        <Button onClick={() => {
                            this.inputRef.current.focus();
                        }}>ТЕСТ ФОКУСА</Button><br/>
                        Используем карточки для запоминания и пополняйте активный словарный запаз
                    </Paragraph>
                </HeaderBlock>
                <HeaderBlock hideBackground>
                    <Header>Поучим слова?</Header>
                    <Paragraph>
                        Ниже будут интересные карточки по изучению слов!
                    </Paragraph>
                </HeaderBlock>
                <TestContext.Consumer>
                    {
                        (value) => {
                            console.log(value);
                            return (
                                <CardList
                                    onDeletedItem={this.handleDeletedItem}
                                    onAddItem={this.setNewWord}
                                    inputRef={this.inputRef}
                                    item={wordArr}
                                />
                            );
                        }
                    }
                </TestContext.Consumer>

                <BackgroundBlock
                    color="green"
                >
                    <Paragraph>
                        <StarOutlined />
                        <StarFilled />
                        <StarTwoTone twoToneColor="#00f" />
                        123456
                        <StarTwoTone twoToneColor="#00f" />
                        <StarFilled />
                        <StarOutlined />

                        <>
                            <HomeOutlined />
                            <SettingFilled />
                            <SmileOutlined />
                            <SyncOutlined spin />
                            <SmileOutlined rotate={180} />
                            <LoadingOutlined />
                        </>

                        <ReactLogo />
                    </Paragraph>
                </BackgroundBlock>
                <MainText padding="35">
                    <Header>Lorem ipsum</Header>
                    <Paragraph>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                        culpa qui officia deserunt mollit anim id est laborum. Curabitur
                        pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius,
                        turpis et commodo pharetra, est eros bibendum elit, nec luctus magna
                        felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida.
                        Duis ac tellus et risus vulputate vehicula. Donec lobortis risus a
                        elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros est
                        euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum
                        consequat mi. Donec fermentum. Pellentesque malesuada nulla a mi.
                        Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque.
                        Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing sapien,
                        sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc.
                        Nullam arcu. Aliquam consequat. Curabitur augue lorem, dapibus quis,
                        laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie
                        eu, feugiat in, orci. In hac habitasse platea dictumst.
                    </Paragraph>
                </MainText>
                <FooterBlock
                    lasttext="Footer Block Text"
                    color="lightgray"
                />
            </>
        );
    }
}

HomePage.contextType = FirebaseContext;

const mapStateToProps = (state) => {
    console.log('>> . ', state);
    return {
        countNumber: state.counter.count,
        isBusy: state.cardList.isBusy,
        wordArr: state.cardList.payload || [],
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        ...actions,
        fetchCardList: fetchCardList
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
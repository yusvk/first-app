import React, {PureComponent} from "react";
import Card from "../../components/Card";
import { Typography, Spin } from "antd";
import s from './CurrentCard.module.scss';
import FirebaseContext from "../../context/FirebaseContext";
import LoginPage from "../Login";

const { Title } = Typography;

class CurrentCard extends PureComponent {
    state = {
        word : {
            id: 0,
            eng: '',
            rus: '',
        }
    }

    componentDidMount() {
        const { getUserCurrentCardRef } = this.context;
        const { match : {params} } = this.props;
        if (params.id) {
            getUserCurrentCardRef(params.id).once('value').then(res => {
                this.setState({
                    word: res.val(),
                });
            });
        }
    }

    render() {
        const { word: { eng, rus, id } } = this.state;

        if (eng === '' && rus === '') {
            return <div className={s.root}><Spin /></div>
        }

        return (
            <div className={s.card}>
                <Title>
                    This title is our Current Card - {eng}
                </Title>
                <Card index={id} eng={eng} rus={rus} />
            </div>
        );
    }
}
CurrentCard.contextType = FirebaseContext;

export default CurrentCard;
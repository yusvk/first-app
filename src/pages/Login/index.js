import React, { Component } from "react";
import { Layout, Form, Input, Button } from "antd";

import s from './Login.module.scss';

import FirebaseContext from "../../context/FirebaseContext";

const { Content } = Layout;

class LoginPage extends Component {
    state = {
        msg: null,
    }

    handleLogin = (email, password) => {
        const { signWithEmail, setUserUid } = this.context;
        const { msg } = this.state;
        const { history } = this.props;

        (email && password) && signWithEmail(email, password)
            .then(res => {
                console.log('loading... ', res);
                setUserUid(res.user.uid);
                localStorage.setItem('user', res.user.uid);
                history.push('/');
            }, rej => {
                this.setState({msg : 'Is\'t correct input data!'});
            });
    }

    onFinish = ({ email, password, regmail, regpass }) => {
        const { registrationUser } = this.context;
        const { msg } = this.state;

        (email && password) && this.handleLogin(email, password);

        (regmail && regpass) && registrationUser(regmail, regpass)
            .then(res => {
                console.log('loading... ', res);
                this.handleLogin(email, password);
            }, rej => {
                this.setState({msg : rej.message});
            });
    }

    onFinishFailed = (errorMsg) => {
        console.log('::errMsg ', errorMsg);
    }

    renderForm = (action) => {
        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };

        const tailLayout = {
            wrapperCol: { offset: 6, span: 18 },
        };

        const nameForm = {
            email : action !== 'Login' ? 'regmail' : 'email',
            pass : action !== 'Login' ? 'regpass' : 'password',
        }

        return (
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={ this.onFinish }
                onFinishFailed={ this.onFinishFailed }
            >
                <Form.Item
                    label="Email"
                    name={nameForm.email}
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name={nameForm.pass}
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button
                        type="primary"
                        htmlType="submit"
                    >
                        {action}
                    </Button>
                </Form.Item>
            </Form>
        );
    }

    render() {
        const {msg} = this.state;
        return (
            <Layout>
                <Content>
                    <div className={s.info_message}>
                        {msg}
                    </div>
                    <div className={s.root}>
                        <div className={s.form_wrap}>
                            { this.renderForm('Login') }
                        </div>
                        <div className={s.form_wrap}>
                            { this.renderForm('Registration') }
                        </div>
                    </div>
                </Content>
            </Layout>
        );
    }
}

LoginPage.contextType = FirebaseContext;

export default LoginPage;
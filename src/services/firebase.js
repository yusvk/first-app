import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';


const FB_API_KEY = process.env.REACT_APP_FB_API_KEY;
const FB_AUTH_DOMAIN = process.env.REACT_APP_FB_AUTH_DOMAIN;
const FB_DATABASE_URL = process.env.REACT_APP_FB_DATABASE_URL;
const FB_PROJECT_ID = process.env.REACT_APP_FB_PROJECT_ID;
const FB_MESSAGING_SENDER_ID = process.env.REACT_APP_FB_MESSAGING_SENDER_ID;
const FB_STORAGE_BUCKET = process.env.REACT_APP_FB_STORAGE_BUCKET;
const FB_APP_ID = process.env.REACT_APP_FB_APP_ID;

const firebaseConfig = {
    apiKey: FB_API_KEY,
    authDomain: FB_AUTH_DOMAIN,
    databaseURL: FB_DATABASE_URL,
    projectId: FB_PROJECT_ID,
    storageBucket: FB_STORAGE_BUCKET,
    messagingSenderId: FB_MESSAGING_SENDER_ID,
    appId: FB_APP_ID
};

class Firebase {
    constructor() {
        firebase.initializeApp(firebaseConfig);

        this.auth = firebase.auth();
        // this.registration = firebase. .sig00();
        this.database = firebase.database();

        this.userUid = null;
    }

    setUserUid = (uid) => this.userUid = uid;

    signWithEmail = (email, password) => this.auth.signInWithEmailAndPassword(email, password);

    getUserCardsRef = () => this.database.ref(`${this.userUid}`);

    logOut = () => this.auth.signOut();

    registrationUser = (regmail, regpass) => this.auth.createUserWithEmailAndPassword(regmail, regpass);

    getUserCurrentCardRef = (id) => this.database.ref(`/${this.userUid}/${id}`);
}

export default Firebase;